const express = require("express");
require("dotenv").config();
const db = require("./db/db");
const bodyParser = require("body-parser");
const cors = require('cors');

//Create app
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//Create swagger
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

//Public dir
app.use("/public", express.static(process.env.PUBLIC_DIR));

// Enable CORS
app.use(cors());

// Connection DB
db.connect()
    .then(() => {
        console.log('DB Connected Success');
    })
    .catch((err) => {
        console.log(err);
    });

app.use("/api/v1", require("./routes/v1"));

app.listen(process.env.PORT, function() {
    console.log(`Node server listening on port ${process.env.PORT}`);
});
