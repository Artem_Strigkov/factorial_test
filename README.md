## Start a project on a local device

1. Turn on **ngrok** on 3000 PORT.
2. Get url address with https protocol (https protocol is important for interacting with telegram)
3. Rename .env.example to .env
4. Save this url as HOST param in **.env** file
5. Run **npm install** in terminal
6. Then run **npm run dev**

---

**This is server side or API. You can look it on swagger on route localhost:3000/api-docs**

---

## Todo list task

1. This part consists of models: todo (tasks) and list.
2. The basic functionality is CRUD for both models + functionality for checking the existence of a task or list
3. The creation of a todo and a list is implemented through a post request.
4. There can not be task without a list.
5. If there is no listId when creating a task, a separate list is created for this task.
6. Obtaining information about todo and lists is implemented through a get request.
7. Filtering works with get parameters listId, id for todo and id for list.
8. Updating todo can be in date, listId and description params. Updating list can be only on listName
9. You can delete todo and list by id, but if will delete list todo which have current listId will be removed.
 

---

## Telegram bot

1. This chat bot works on @ArtemVocabulary_bot
2. After you set the HOST value and start server (npm run dev) you can set webhook no telegram API for you HOST
3. This can be done by sending a post request to the address http://localhost:3000/api/v1/webhook/set
4. You can also do this in swagger.
5. As soon as the webhook is installed, telegram will be able to send data about received messages to the server.
6. Then on post route {{HOST}}/api/v1/webhook will be get message information and send answer. 

**Remember that the webhook must be installed on an address with a https protocol.**

---
