const { sender } = require("../helpers/telegramSender");

module.exports = {
    async setWebhook(req, res, next) {
        await sender.setWebhook();
        res.status(200).send("WEBHOOK WAS SET");
    },

    async get(req, res, next) {
        res.status(200).send("OK");
    },

    async post(req, res, next) {
        //GET MESSAGE DATA FROM TELEGRAM
        const message = req.body.message.text;
        const senderId = req.body.message.from.id;
        //
        let mewMessage= message.split("").reverse().join("");

        sender.sendMessage(senderId, mewMessage);
        res.status(200).send("OK");
    }
};
