const express = require('express');
const todoController = require('../../controllers/todo');
const router = express.Router();

//================================= ROUTES ==================================
router.get('/', todoController.listing);
router.post('/', todoController.add);
router.patch('/', todoController.update);
router.delete('/', todoController.remove);

module.exports = router;
