const ObjectId = require('mongoose').Types.ObjectId;
const { listDB, todoDB }  = require("../helpers/db");
const getRequestParams = require("../helpers/getRequestParams");


module.exports = {
    // ======================= LISTING ============================
    async listing(req, res, next) {
        try {
            const searchObj = getRequestParams(req.query, ["listName", "id"]);
            const DBResponse = await listDB.findAll(searchObj);

            res.status(200).send({ msg: "ALL LIST", DBResponse });

        } catch (err) {
            console.log("controllers/list", err);
            res.status(400).send({ msg: "ERROR", err });
        }
    },

    // ======================= CREATE LIST ============================
    async add(req, res, next) {
        try{
            const listObj = getRequestParams(req.body, ["listName"]);

            // CHECK LIST NAME
            if(!listObj.listName || listObj.listName === "") res.status(200).send({ msg: "EMPTY LIST NAME" });

            // SAVE LIST
            const DBResponse = await listDB.create(listObj);

            res.status(200).send({ msg: "LIST SAVED", DBResponse });

        } catch (err) {
            console.log("controllers/list", err);
            res.status(400).send({ msg: "ERROR", err });
        }
    },

    // ======================= UPDATE LIST ============================
    async update(req, res, next) {
        try{
            const filterParam = getRequestParams(req.body, ["id"]);
            const updateObj = getRequestParams(req.body, ["listName"]);

            //CHECK LIST ID
            if(!ObjectId.isValid(filterParam._id)) return res.status(400).send({ msg: "INVALID _ID PARAM" });

            //CHECK LIST
            const CheckListResponse = await listDB.findOne(filterParam);
            if(!CheckListResponse) return res.status(400).send({ msg: "LIST IS NOT EXIST" });

            //UPDATE LIST
            const DBResponse = await listDB.update(filterParam, updateObj);

            //GET ACTUAL LIST
            const actualList = await listDB.findOne(filterParam);
            return res.status(200).send({ msg: "LIST UPDATE", DBResponse: actualList });

        } catch (err) {
            console.log("controllers/list", err);
            return res.status(400).send({ msg: "ERROR", err });
        }
    },

    // ======================= REMOVE LIST WITH ALL TASKS ============================
    async remove(req, res, next) {
        try {
            const filterParam = getRequestParams(req.body, ["id"]);

            //CHECK LIST ID
            if(!ObjectId.isValid(filterParam._id)) return res.status(400).send({ msg: "INVALID _ID PARAM" });

            //CHECK LIST
            const CheckListResponse = await listDB.findOne(filterParam);
            if(!CheckListResponse) return res.status(400).send({ msg: "LIST IS NOT EXIST" });

            // REMOVE ALL TASK ON THIS LIST
            await todoDB.deleteMany({listId: filterParam._id});

            //REMOVE LIST
            await listDB.delete(filterParam);
            res.status(200).send({ msg: "LIST IS REMOVE" });

        } catch (err) {
            console.log("controllers/list", err);
            res.status(400).send({ msg: "ERROR", err });
        }
    }
};
