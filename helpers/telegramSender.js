const axios = require('axios');
const host = process.env.HOST;
const token = process.env.TELEGRAM_TOKEN;

class TelegramSender {
    constructor() {
        this.requestToTelegram = (token, data, telegramMethod) => {
            const config = {
                method: 'post',
                url: `https://api.telegram.org/bot${token}/${telegramMethod}`,
                headers: {
                    'Content-Type': 'application/json'
                },
                data : data
            };

            axios(config)
                .then(function (response) {
                    console.log(JSON.stringify(response.data));
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    };

    setWebhook = () => {
        const data = JSON.stringify({"url":`${host}/api/v1/webhook`});
        this.requestToTelegram(token, data, "setWebhook");
    };

    sendMessage = (senderId, text) => {
        const data = JSON.stringify({"chat_id": senderId ,"text": text});
        this.requestToTelegram(token, data, "sendMessage");
    }
}

const sender = new TelegramSender();

module.exports = { sender };
