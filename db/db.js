const mongoose = require('mongoose');

class MongoDB {
  constructor() {
    this.connector = mongoose;
    this.dbURL = process.env.MONGO_CONNECT_URL;
    this.db = null;
  }

  async connect() {
    try {
      this.db = await this.connector.connect(this.dbURL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      });
      return true;
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async getDBInstance() {
    if (!this.db) {
      await this.connect();
    }
    return this.connector;
  }
}

module.exports = new MongoDB();
