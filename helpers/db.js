const mongoose = require('mongoose');
require("../models/todo");
require("../models/list");

class DB {
    constructor(collection) {
        this.findOne = (searchObj) => {
            return mongoose.model(collection).findOne(searchObj);
        };
        this.findAll = (searchObj) => {
            return mongoose.model(collection).find(searchObj);
        };
        this.create = (creationObj) => {
            return mongoose.model(collection).create(creationObj);
        };
        this.update = (filter, updateObj) => {
            return mongoose.model(collection).updateMany(filter, updateObj);
        };
        this.delete = (filter) => {
            return mongoose.model(collection).deleteOne(filter);
        };
        this.deleteMany = (filter) => {
            return mongoose.model(collection).deleteMany(filter);
        };
    }
}

const todoDB = new DB("todo");
const listDB = new DB("list");

module.exports = { todoDB, listDB };
