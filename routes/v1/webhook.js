const express = require('express');
const webhookController = require('../../controllers/webhook');
const router = express.Router();

//================================= ROUTES ==================================
router.post('/set', webhookController.setWebhook);
router.get('/', webhookController.get);
router.post('/', webhookController.post);

module.exports = router;
