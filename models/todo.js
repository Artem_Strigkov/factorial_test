const mongoose = require('mongoose');

const todo = new mongoose.Schema({
  date: {
    type: Date,
    required: true
  },
  listId: {
    type: String,
    required: false
  },
  description: {
    type: String,
    required: true
  }
});

mongoose.model('todo', todo);

module.exports = mongoose.model('todo');
