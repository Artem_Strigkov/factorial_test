const ObjectId = require('mongoose').Types.ObjectId;
const { todoDB, listDB }  = require("../helpers/db");
const getRequestParams = require("../helpers/getRequestParams");


module.exports = {
    // ======================= TASK LISTING ============================
    async listing(req, res, next) {

        try {
            const searchObj = getRequestParams(req.query, ["listId", "id"]);
            const DBResponse = await todoDB.findAll(searchObj);

            res.status(200).send({ msg: "ALL TODO", DBResponse });

        } catch (err) {
            console.log("controllers/todo", err);
            res.status(400).send({ msg: "ERROR", err });
        }
    },

    // ======================= CREATE TASK ============================
    async add(req, res, next) {
        try{
            let msg = "TODO SAVED";
            const todoObj = getRequestParams(req.body, ["date", "listId", "description"]);

            // CHECK DATE
            if(!todoObj.date) todoObj.date = new Date();

            // CHECK DESCRIPTION
            if(!todoObj.description || todoObj.description === "") res.status(200).send({ msg: "EMPTY DESCRIPTION" });

            // CHECK TASK ID
            if(!ObjectId.isValid(todoObj.listId)) return res.status(400).send({ msg: "INVALID listId PARAM" });

            // CHECK LIST
            const currentList = await listDB.findOne({ _id : todoObj.listId });

            // IF LIST IS NOT EXIST CREATE NEW LIST
            if(currentList === null){
                msg = `THE LIST WITH ID ${todoObj.listId} IS NO EXIST WE ARE CREATE NEW LIST FOR THIS TODO`;
                const newList = await listDB.create({listName: `List ${new Date()}`});
                todoObj.listId = newList._id;
            }

            // SAVE TASK
            const DBResponse = await todoDB.create(todoObj);

            res.status(200).send({ msg, DBResponse });

        } catch (err) {
            console.log("controllers/todo", err);
            res.status(400).send({ msg: "ERROR", err });
        }
    },

    // ======================= UPDATE TASK ============================
    async update(req, res, next) {
        try{
            const filterParam = getRequestParams(req.body, ["id"]);
            const updateObj = getRequestParams(req.body, ["date", "listId", "description"]);

            //CHECK TASK ID
            if(!ObjectId.isValid(filterParam._id)) return res.status(400).send({ msg: "INVALID _ID PARAM" });

            //CHECK TASK
            const CheckTodoResponse = await todoDB.findOne(filterParam);
            if(!CheckTodoResponse) return res.status(400).send({ msg: "TODO IS NOT EXIST" });

            //UPDATE TASK
            const DBResponse = await todoDB.update(filterParam, updateObj);

            //GET ACTUAL TASK
            const actualTodo = await todoDB.findOne(filterParam);
            return res.status(200).send({ msg: "TODO UPDATE", DBResponse: actualTodo });

        } catch (err) {
            console.log("controllers/todo", err);
            return res.status(400).send({ msg: "ERROR", err });
        }
    },

    // ======================= REMOVE TASK ============================
    async remove(req, res, next) {
        try {
            const filterParam = getRequestParams(req.body, ["id"]);

            //CHECK TASK ID
            if(!ObjectId.isValid(filterParam._id)) return res.status(400).send({ msg: "INVALID _ID PARAM" });

            //CHECK TASK
            const CheckTodoResponse = await todoDB.findOne(filterParam);
            if(!CheckTodoResponse) return res.status(400).send({ msg: "TODO IS NOT EXIST" });

            //REMOVE TASK
            await todoDB.delete(filterParam);
            res.status(200).send({ msg: "TODO IS REMOVE" });

        } catch (err) {
            console.log("controllers/todo", err);
            res.status(400).send({ msg: "ERROR", err });
        }
    }
};
