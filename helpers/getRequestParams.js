// request.query or request.body
const getRequestParams = (request, params) => {
    const requestParams = {};
    params.forEach((param, index, params) => {
        if(request[param]){
            if(param === "id"){
                requestParams._id = request[param];
            } else {
                requestParams[param] = request[param];
            }
        }
    });

    return requestParams;
};

module.exports = getRequestParams;
