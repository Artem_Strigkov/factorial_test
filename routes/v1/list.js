const express = require('express');
const listController = require('../../controllers/list');
const router = express.Router();

//================================= ROUTES ==================================
router.get('/', listController.listing);
router.post('/', listController.add);
router.patch('/', listController.update);
router.delete('/', listController.remove);

module.exports = router;
