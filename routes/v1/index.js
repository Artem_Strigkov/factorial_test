const express = require("express");

var router = express.Router();

router.use("/todo", require("./todo"));
router.use("/list", require("./list"));
router.use("/webhook", require("./webhook"));

module.exports = router;
