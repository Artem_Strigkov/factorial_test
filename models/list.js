const mongoose = require('mongoose');

const todo = new mongoose.Schema({
  listName: {
    type: String,
    required: true
  }
});

mongoose.model('list', todo);

module.exports = mongoose.model('list');
